<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 

class Lib_login {
	// SET SUPER GLOBAL
	var $CI = NULL;
	public function __construct() {
		$this->CI =& get_instance();
	}
	
	public function login_member($email, $pasword) {
		// Query untuk pencocokan data
		$query = $this->CI->db->get_where('usr_member', array(
										'email' => $email, 
										'password' => $pasword
										));
										
		// Jika ada hasilnya
		if($query->num_rows() == 1) {
			$row 	= $this->CI->db->query('SELECT * FROM usr_member where email = "'.$email.'"');
			$admin 	= $row->row();
			$id 	= $admin->id_member;
			$nama 	= $admin->nama_member;
			// $_SESSION['email'] = $email;
			$this->CI->session->set_userdata('nama_member', $nama); 
			$this->CI->session->set_userdata('id_login', uniqid(rand()));
			$this->CI->session->set_userdata('id', $id);
			// Kalau benar di redirect
			redirect(base_url());
		}else{
			$this->CI->session->set_flashdata('failed_login','Oopss.. email/password salah');
			redirect(base_url().'member');
		}
		return false;
	}
	public function cek_login_member() {
		if($this->CI->session->userdata('nama_member') == '') {
			redirect('member');
		}
	}
	
	// Logout
	public function logout() {
		$this->CI->session->unset_userdata('email');
		$this->CI->session->unset_userdata('nama_member');
		$this->CI->session->unset_userdata('id_login');
		$this->CI->session->unset_userdata('id');
		// $this->cart->destroy();
		$this->CI->session->set_flashdata('logout_success','Terimakasih, Anda berhasil logout');
		redirect(base_url());
	}
}