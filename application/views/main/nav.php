    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url() ?>">RCDev Games</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?php echo base_url() ?>list_game"><i class="fa fa-list"></i> List Game</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>cara_order"><i class="fa fa-question"></i> Cara Order</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>contact"><i class="fa fa-user"></i> Contact</a>
                    </li>
                </ul>
                <?php if($this->session->userdata('nama_member')==FALSE) { ?>
                <ul class="nav navbar-nav navbar-right">    
                    <li class="dropdown">
                        <!--<a href="<?php echo base_url() ?>member">Login</a>-->
                        <a id="drop3" href="<?php echo base_url() ?>member" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-lock"> </span> Login</a>
                        <ul class="dropdown-menu" aria-labelledby="drop3" style="width:250px;">
                            <li>
                                <form style="margin: 0px" accept-charset="UTF-8" action="<?php echo base_url()?>member/login" method="post"><div style="margin:0;padding:0;display:inline"></div>
                                 <fieldset class='textbox' style="padding:10px">
                                    <h5 style="text-align:center;">LOG IN</h5><hr style="margin-top: 3px;margin-bottom: 2px;">
                                    <input class="form-control" style="margin-top: 8px" type="text" placeholder="Email" id="email" name="email">
                                    <input class="form-control" style="margin-top: 8px" type="password" placeholder="Passsword" id="pasword" name="pasword">
                                    <button class="btn btn-success btn-sm pull-right" id="loginButton" data-loading-text="Loading..." autocomplete="off" style="margin-top: 3px;">Login</button>
                                </fieldset>
                                </form>
                                <hr style="margin-top: 1px;margin-bottom: 2px;">
                                <p style="text-align:center;">Can't Order? <a href="<?php echo base_url() ?>member">Sign up</a></p>
                            </li>
                        </ul>                       
                    </li> 
                </ul>
                <?php }else{ ?>
                <ul class="nav navbar-nav navbar-right">
                    <li id="fat-menu" class="dropdown">
                    <a id="drop3" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class='fa fa-shopping-cart'></i> <?php echo $this->cart->total_items(); ?> | <?php echo $this->session->userdata('nama_member'); ?>
                    <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="drop3">
                        <li><a href="<?php echo base_url() ?>cart"><i class='fa fa-shopping-cart'></i> My Cart</a></li>
                        <li><a href="<?php echo base_url() ?>cart/history"><i class='fa fa-list-alt'></i> My Transaction</a></li>
                        <li><a href="<?php echo base_url() ?>member/profile"><i class='fa fa-user'></i> Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo base_url() ?>member/logout"><i class='fa fa-sign-out'></i>  Logout</a></li>
                    </ul>
                    </li>
                </ul>
                <?php } ?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

