<div class="container">

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12" style="text-align:right">
                    <hr><p>Copyright &copy; RCDev Games <?php echo date("Y"); ?></p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.js"></script>
    

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>

</body>

</html>
