           <div class="col-md-9">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>">Home</a></li>
                    <li class="active">Cara Order</li>
                </ol>

                <h1>CARA ORDER</h1><hr/>
               	<p>1. Pembeli haruslah menjadi member, bila belum menjadi member anda dapat mendaftarkan diri anda <a href="member">disini</a>.</p>
               	<p>2. Pilih barang yang ingin anda beli.</p>
               	<p>3. Tulis dengan lengkap data diri serta alamat lengkap agar barang yang di pesan dapat di kirim.</p>
               	<p>4. Transfer dengan jumlah yang tertera di halaman Pre-Order ke nomor rekening yang sudah tertera di member area.</p>
               	<p>5. Konfirmasikan bukti transfer dengan cara yang tertera di member area.</p>
               	<p>6. Tunggu barang sampai di rumah anda dengan selamat. ^_^</p>
               	<br/>
               	<p>NOTE :</p> 
               	<p>
               	- HARGA BARANG SUDAH NETT (NO NEGO !)<br/>
               	- HARGA BARANG BELUM TERMASUK ONGKOS KIRIM !<br/>
               	- BARANG YANG SUDAH DIBELI TIDAK DAPAT DITUKAR ATAUPUN DI KEMBALIKAN KECUALI ADA PERJANJIAN SEBELUMNYA.<br/>
               	- SEMUA BARANG YANG DIKIRIM SUDAH DI TEST TERLEBIH DAHULU DAN SUKSES/TIDAK CACAT.
               	</p>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->
