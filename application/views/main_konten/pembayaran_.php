<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-1.7.2.min.js"></script>
<script type="application/javascript">
  function isNumberKeyTrue(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 65) {
            alert("Maaf, hanya diperbolehkan menggunakan angka !!!");
            return false;
         } else {
            return true;
         }
      }
</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#nama").focus();

    $('#Telpon').keyup(function(e) {
    if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode < 106 && e.keyCode > 95)) {
        this.value = this.value.replace(/(\d{4})\-?/g, '$1-');
        return true;
    }
  
    //remove all chars, except dash and digits
    this.value = this.value.replace(/[^\-0-9]/g, '');
    });

    $("#lokasi_propinsi").change(function(){
        $("#lokasi_kecamatan").val('');
        var lokasi_propinsi = $("#lokasi_propinsi").val();
        $.ajax({
            type : "POST",
            url  : "<?php echo base_url() ?>lokasi/getkabkot",
            data : "lokasi_propinsi=" + lokasi_propinsi,
            success : function(data) {
                $("#lokasi_kabupatenkota").html(data);
            }
        });
    });

    $("#lokasi_kabupatenkota").change(function(){
        var lokasi_kabupatenkota = $("#lokasi_kabupatenkota").val();
        $.ajax({
            type : "POST",
            url  : "<?php echo base_url() ?>lokasi/getkec",
            data : "lokasi_kabupatenkota=" + lokasi_kabupatenkota,
            success : function(data) {
                $("#lokasi_kecamatan").html(data);
            }
        });
    });
});
</script>

<div class="col-md-9">
<div id="dump">
<?php 
// $lokasi_propinsi = '31';
// $lokasi_kabupatenkota = $this->input->post('lokasi_kabupatenkota');
// $query = $this->main_model->getKec($lokasi_kabupatenkota); 
//return $query->row_array(); 
// echo var_dump($query);
// print_r($query);
?>
</div>

<?php if ($cart = $this->cart->contents()): ?>
<?php
$grand_total = 0; $i = 1;
        
foreach ($cart as $item):
echo form_hidden('cart['. $item['id'] .'][id]', $item['id']);
echo form_hidden('cart['. $item['id'] .'][rowid]', $item['rowid']);
echo form_hidden('cart['. $item['id'] .'][name]', $item['name']);
echo form_hidden('cart['. $item['id'] .'][price]', $item['price']);
echo form_hidden('cart['. $item['id'] .'][qty]', $item['qty']);
$grand_total = $grand_total + $item['subtotal'];
endforeach;
?>

<script type="text/javascript">
$(document).ready(function(){
    $("#kurir").change(function(){
        var kurir = $("#kurir").val();
        if (kurir=="") {
            $("#ongkir").text("0");
            $("#grand_total").text("<?php echo number_format($grand_total,0,',','.'); ?>");
        }else{
        $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>order/proses",
                data : "kurir="+kurir,
                success : function(data) {
                    // body...
                }
            });
        }
        //var REG = "<?php echo number_format($ongkir = $grand_total,0,',','.'); ?>";
        //$("#type_kurir").text(kurir);
        //$("#ongkir").text(REG);
        //$("#grand_total").text("<?php echo number_format($grand_total + $ongkir,0,',','.'); ?>");
    });

    $("#id_alamat").val('');
    $('#detail-alamat').hide();
    $("#id_alamat").change(function(){
        var id_alamat = $("#id_alamat").val();
        $.ajax({
                type : "POST",
                url  : "<?php echo base_url()?>alamat/detail_alamat",
                data : "id_alamat=" + id_alamat,
                success : function(data){
                    $("#isi-alamat").html(data);
                }
            });
        if (id_alamat=="") {
            $('#detail-alamat').hide();
            $("#form-alamat").show();
        } else{
            $("#form-alamat").hide();
            $("#detail-alamat").show();
        };
        $("#kurir").val('');
        $("#type_kurir").text('');
        $("#ongkir").text("0");
        $("#grand_total").text("<?php echo number_format($grand_total,0,',','.'); ?>");
    });
});
</script>


    <input class="btn btn-default pull-right" type="button" value="Kembali ke Shopping cart" onclick="window.location='<?php echo base_url() ?>cart'" style="margin-top:20px;" />
    <h1>Form Data Pembeli</h1>
    <hr/>
    <div style="margin:0px auto; width:100%;" >
        <select class="form-control" id="id_alamat" name="id_alamat">
            <option value="">Alamat Baru</option>
        <?php foreach ($alamat as $q) { ?>
            <option value="<?php echo $q['id_alamat'] ?>"><?php echo $q['nama'] ?></option>
        <?php } ?>
        </select><br>
        <div class="alert alert-warning" role="alert"><center><b style="font-size:20px;">Kami menjamin kerahasian data anda saat bertransaksi dengan kami</b></center></div>
        <hr>
        <form role="form" method="POST" action="<?php echo base_url()?>order">
            <div class="row">
                <div class="col-md-8" id="form-alamat" style="border-right:5px solid #eee;">
                  <div class="form-group">
                    <label for="id_pesanan">Nomor Order</label>
                    <input type="text" class="form-control" name="id_pesanan" id="id_pesanan" readonly value="<?php echo date("Ym"),$this->session->userdata('id'),date("i"),sprintf("%02d", $this->cart->total_items());  ?>">
                  </div>
                  <div class="form-group">
                    <label for="Nama">Nama</label>
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama">
                  </div>
                  <div class="form-group">
                    <label for="Email">Email address</label>
                    <input type="email" class="form-control" name="email" id="Email" placeholder="Masukan email">
                  </div>
                  <div class="form-group">
                    <label for="Telpon">Telpon/Handphone</label>
                    <input type="text" class="form-control" name="telpon" id="Telpon" onkeypress="return isNumberKeyTrue(event)" placeholder="Masukan Telpon/Handphone">
                  </div>
                  <div class="form-group">
                    <label for="lokasi_propinsi">Provinsi</label>
                    <select class="form-control" name="lokasi_propinsi" id="lokasi_propinsi">
                        <option>Pilih Provinsi</option>
                        <?php foreach ($provinsi as $provinsi) { ?>
                        <option value="<?php echo $provinsi['lokasi_propinsi'] ?>"><?php echo $provinsi['lokasi_nama'] ?></option>
                        <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="lokasi_kabupatenkota">Kota/Kabupaten</label><div id="img-kab"></div>
                    <select class="form-control" name="lokasi_kabupatenkota" id="lokasi_kabupatenkota">
                        <option>Pilih Kota/Kabupaten</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="lokasi_kecamatan">Kecamatan</label> <!--<img src="<?php echo base_url() ?>assets/img/loader.gif" style="margin-left:2px;" id="loader1" alt="" />-->
                    <select class="form-control" name="lokasi_kecamatan" id="lokasi_kecamatan">
                        <option value="">Pilih Kecamatan</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat Lengkap</label>
                    <textarea name="alamat" class="form-control" rows="3"></textarea>
                  </div>
                </div>

                <div class="col-md-8" id="detail-alamat"  style="border-right:5px solid #eee;">
                    <div class="form-group">
                        <label for="id_pesanan">Nomor Order</label>
                        <input type="text" class="form-control" name="id_pesanan" id="id_pesanan" readonly value="<?php echo date("Ym"),$this->session->userdata('id'),date("i"),sprintf("%02d", $this->cart->total_items());  ?>">
                    </div>
                    <div id="isi-alamat"></div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <center><img class="img-responsive" src="http://localhost/rcdev/assets/img/tiki.png"></center><br/>
                        <select class="form-control" name="kurir" id="kurir">
                            <option value="">Pilih Jenis Pengiriman</option>
                            <option value="OKE">OKE</option>
                            <option value="REG">REG</option>
                            <option value="YES">YES</option>
                        </select>
                        <hr/>
                        <h3>Daftar Belanja</h3>
                        <b class="pull-right">Subtotal</b>
                        <b>Barang</b><hr/>
                        <?php foreach ($cart as $item): ?>
                        <div class="pull-right">
                            <b>Rp. <?php echo number_format($item['subtotal'],0,',','.') ?></b>
                        </div>
                        <div class="Pull-left">
                            <b><?php echo $item['name']; ?></b><br/>
                            <b><?php echo $item['qty']; ?></b> barang
                            <p>@Rp. <?php echo number_format($item['price'],0,',','.'); ?></p>
                        </div>
                        <?php endforeach; ?>
                        <hr/>
                        <b class="pull-right">Rp. <?php echo number_format($grand_total,0,',','.'); ?></b>
                        <p>Total (<b><?php echo $this->cart->total_items(); ?></b> barang)</p>    
                        <b class="pull-right">Rp. <b id="ongkir">0</b></b>
                        <p>Ongkos Kirim (JNE-<a id="type_kurir"></a>)</p>
                        <hr/>
                        <b class="pull-right" id="total">Rp. <a id="grand_total"><?php echo number_format($grand_total,0,',','.'); ?></a></b>
                        <p>TOTAL : </p>
                    </div>
                </div> 
            </div><hr>
            <div class="pull-right">
                <button id="btnBack" onclick="window.location.href='<?php echo base_url() ?>cart'" class="btn btn-default btn-lg">KEMBALI KE CART</button>
                <button id="btnSave" onclick="save()" class="btn btn-primary btn-lg">LANJUT</button>
            </div>
        </form>
    </div>
<?php endif; ?>
</div>
<!-- /.container -->