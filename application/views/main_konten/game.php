<div class="col-md-9">

                <div class="thumbnail" style="padding-right: 12px;padding-left: 12px;">
                    <img style="width:70%; height: 70%;>" class="img-responsive" src="<?php echo base_url().'assets/img/upload/'.$data['gambar'] ?>" alt="">
                    <div class="caption-full">
                        <?php
                            if($this->session->userdata('nama_member')!=FALSE) 
                            {
                            echo form_open(base_url().'cart/add');
                            echo form_hidden('id', $data['id_produk']);
                            echo form_hidden('name', $data['nama_prod']);
                            echo form_hidden('price', $data['harga']);
                            echo '<button type="submit" class="btn btn-success btn-sm pull-right" style="margin-top: 13px;"><span class="fa fa-shopping-cart"></span> Beli - Rp '.number_format($data['harga'],0,',','.') .'</button>';
                            echo form_close();
                            }else{ ?>
                            <button class="btn btn-success btn-sm pull-right" onclick="window.location.href='<?php echo base_url()?>member'"><span class="fa fa-shopping-cart"></span> Beli - Rp <?php echo number_format($data['harga'],0,',','.') ?></button>
                            <?php } ?>
                        <h4><?php echo $data['nama_prod'] ?></h4>
                        <p><?php echo $data['deskripsi'] ?></p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right"><?php echo $data['review'] ?> reviews</p>
                        <p>
                                <?php if($data['review']>=0 && $data['review']<=200){ ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($data['review']>=200 && $data['review']<=400) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($data['review']>=400 && $data['review']<=600) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($data['review']>=600 && $data['review']<=800) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($data['review']>=1000) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                <?php }else{ ?>

                                <?php } ?>
                                </p>
                    </div>
                </div>

                <div class="well">
                    <div class="text-right">
                        <a class="btn btn-success" href="#">Leave a Review</a>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">
                            <?php
                                $datestring = "Year: %Y Month: %m Day: %d - %h:%i %a";
                                $time = now();

                                echo mdate($datestring, $time); 
                                echo $time;
                                 ?> Ago</span>
                            <p>This product was great in terms of quality. I would definitely buy another!</p>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">12 days ago</span>
                            <p>I've alredy ordered another one!</p>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star-empty"></span>
                            Anonymous
                            <span class="pull-right">15 days ago</span>
                            <p>I've seen some better than this, but not at this price. I definitely recommend this item.</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->