<div class="col-md-9">
    <h1>PROFILE</h1><hr/>

    <div class="col-md-3">
        <div class="list-group">
            <a class="list-group-item">Change Password</a>
        </div>
    </div>
    <div class="col-md-9"><blackquote>
        <h3 style="margin-top: 7px;">CHANGE PASSWORD</h3><hr>
        <?php 
        echo validation_errors('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>','</div>'); 
        if($this->session->flashdata('gagal')) { ?>
        <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p><i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('gagal')?> </p>
        </div>
        <?php }elseif($this->session->flashdata('berhasil')){ ?>
        <div class="alert alert-success alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <p><i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('berhasil')?> </p>
        </div>
        <?php } ?>
        <form action="<?php echo base_url()?>member/changepass" method="POST" name="form_change_pass">
            <div class="from-group">
                <label>Old Password</label>
                <input type="password" class="form-control" name="oldpass">
            </div>
            <div class="from-group">
                <label>New Password</label>
                <input type="password" class="form-control" name="newpass">
            </div>
            <div class="from-group">
                <label>Confrim New Password</label>
                <input type="password" class="form-control" name="confirpass">
            </div>
            <input type="hidden" name="id_member" value="<?php echo $this->session->userdata('id') ?>">
            <button class="btn btn-success" style="margin-top:5px;" name="submit" id="submit">Submit</button>
        </from>
    </blackquote></div>
</div>
<!-- /.container -->
