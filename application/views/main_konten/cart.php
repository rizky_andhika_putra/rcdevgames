<script type="application/javascript">
  function isNumberKeyTrue(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 65) {
            alert("Maaf, hanya diperbolehkan menggunakan angka !!!");
            return false;
            $("#qty[1]").focus();
         } else {
            return true;
         }
      }
</script>
<div class="col-md-9">
    
    <input class="btn btn-default pull-right" type="button" value="Continue Shopping" onclick="window.location='<?php echo base_url() ?>list_game'" style="margin-top:20px;" />
    <h1>My Cart</h1>
    <hr/>
    <div style="margin:0px auto; width:100%;" >
    <h4 style="color:#F00; text-align:center;"><?php echo $message?></h4>
    <table class="table" border="0" cellpadding="0" cellspacing="1px" style="font-family:Verdana, Geneva, sans-serif; font-size:11px; background-color:#E1E1E1" width="100%">
        <?php if ($cart = $this->cart->contents()): ?>
        <tr bgcolor="#FFFFFF" style="font-weight:bold">
            <td width="5%">Serial</td>
            <td width="35%">Name</td>
            <td width="20%">Price</td>
            <td width="10%">Qty</td>
            <td width="20%">Amount</td>
            <td width="10%">Options</td>
        </tr>
        <?php
        echo form_open(base_url().'cart/update');
        $grand_total = 0; $i = 1;
        
        foreach ($cart as $item):
            echo form_hidden('cart['. $item['id'] .'][id]', $item['id']);
            echo form_hidden('cart['. $item['id'] .'][rowid]', $item['rowid']);
            echo form_hidden('cart['. $item['id'] .'][name]', $item['name']);
            echo form_hidden('cart['. $item['id'] .'][price]', $item['price']);
            echo form_hidden('cart['. $item['id'] .'][qty]', $item['qty']);
        ?>
        <tr bgcolor="#FFFFFF">
            <td>
                <?php echo $i++; ?>
            </td>
            <td>
                <?php echo $item['name']; ?>
            </td>
            <td>
                Rp. <?php echo number_format($item['price'],0,',','.'); ?>
            </td>
            <td>
                <?php echo form_input('cart['. $item['id'] .'][qty]', $item['qty'], 'id="qty['.$item['id'].']" class="forms" maxlength="2" onkeypress="return isNumberKeyTrue(event)" maxlength="3" size="1" style="text-align:center;"'); ?>
            </td>
                <?php $grand_total = $grand_total + $item['subtotal']; ?>
            <td>
                Rp. <?php echo number_format($item['subtotal'],0,',','.') ?>
            </td>
            <td>
                <?php echo anchor(base_url().'cart/remove/'.$item['rowid'],'Cancel'); ?>
            </td>
            <?php endforeach; ?>
        </tr>
        <tr>
            <td></td>
            <td><b style="font-size:15px;">Order Total: <a>Rp. <?php echo number_format($grand_total,0,',','.'); ?></a></b></td>
            <td colspan="5" align="right">
                    <!-- <input type="button" class="btn btn-primary btn-sm" value="Clear Cart" onclick="clear_cart()"> -->
                    <button type="submit" class="btn btn-primary btn-xs">Update Cart</button>
                    <?php echo form_close(); ?>
                    <button class="btn btn-primary btn-xs" onclick="clear_cart()">Clear Cart</button>
                    <button class="btn btn-primary btn-xs" onclick="window.location='cart/pembayaran'">Lanjut ke pembayaran</button></td>
        </tr>
        <?php endif; ?>
    </table>
</div>
</div>
<!-- /.container -->
<script>
function clear_cart() {
    var result = confirm('Batalkan semua pesanan?');
    
    if(result) {
        window.location = "<?php echo base_url(); ?>cart/remove/all";
    }else{
        return false; // cancel button
    }
}
</script>