    <section id="form"><!--form-->
        <div class="container">


            <?php echo validation_errors('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>','</div>'); ?>

            <?php if($this->session->flashdata('failed_login')) { ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('failed_login') ?></p>
            </div>
            <?php } ?>

            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><i class="fa fa-info-circle"></i> You must login first!</p>
            </div>

            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form"><!--login form-->
                        <h2>Login to your account</h2>
                        <form action="<?php echo base_url()?>member/login" method="POST" name="form_login">
                            <input type="email" placeholder="Email Address" name="email" value="<?php echo set_value('email') ?> ">
                            <input type="password" placeholder="Password" name="pasword">
                            <button type="submit_login" class="btn btn-default" id="loginButton" data-loading-text="Loading..." autocomplete="off">Login</button>
                        </form>
                    </div><!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form"><!--sign up form-->
                        <h2>New User Sign Up!</h2>
                        <form action="<?php echo base_url()?>member/register" method="POST" name="form_register">
                            <input name="nama_member" type="text" placeholder="Name" value="<?php echo set_value('nama_member') ?>">
                            <input name="email1" type="email" placeholder="Email Address" value="<?php echo set_value('email1') ?>">
                            <input name="pasword" type="password" placeholder="Password">
                            <input name="pasword1" type="password" placeholder="Confirm Password">
                            <button type="submit_register" class="btn btn-default">Signup</button>
                        </form>
                    </div><!--/sign up form-->
                </div>
            </div>
        </div>
    </section><!--/form-->
