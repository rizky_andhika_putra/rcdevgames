           <div class="col-md-9">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>">Home</a></li>
                    <li class="active">List Game</li>
                </ol>

                <div class="row">

                   <?php foreach ($game as $produk) { ?>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo base_url() .'assets/img/upload/'.$produk['gambar'] ?>" alt=""><hr>
                            <div class="ratings">
                            <h4><a href="<?php echo base_url().'product/index/'.$produk['slug'] ?>" style="padding-right: 10px;padding-left: 0px;color: #000"><?php echo $produk['nama_prod'] ?></a></h4>
                            <h5><?php echo $produk['deskripsi'] ?></h5>
                                <!-- <a type="button" class="btn btn-success btn-sm pull-right" href="<?php if($this->session->userdata('nama_member')==FALSE) { echo base_url().'member'; }else{ echo base_url().''; } ?>"><span class="fa fa-shopping-cart"></span> Beli - Rp <?php echo number_format($produk['harga'],0,',','.') ?></a> -->
                                <?php
                            if($this->session->userdata('nama_member')!=FALSE) 
                            {
                            echo form_open(base_url().'cart/add');
                            echo form_hidden('id', $produk['id_produk']);
                            echo form_hidden('name', $produk['nama_prod']);
                            echo form_hidden('price', $produk['harga']);
                            // $data['class']  = 'btn btn-success btn-sm pull-right';
                            // $data['name']   = 'action';
                            // $data['style']  = 'margin-top: 13px;';
                            // $data['value']  = 'Beli - Rp'.number_format($produk['harga'],0,',','.');
                            // echo form_submit($data);
                            echo '<button type="submit" class="btn btn-success btn-sm pull-right" style="margin-top: 13px;"><span class="fa fa-shopping-cart"></span> Beli - Rp '.number_format($produk['harga'],0,',','.') .'</button>';
                            echo form_close();
                            }else{ ?>
                            <!-- <a type="button" class="btn btn-success btn-sm pull-right" href="<?php echo base_url()?>member"><span class="fa fa-shopping-cart"></span> Beli - Rp <?php echo number_format($produk['harga'],0,',','.') ?></a> -->
                            <button class="btn btn-success btn-sm pull-right" onclick="window.location.href='<?php echo base_url()?>member'"><span class="fa fa-shopping-cart"></span> Beli - Rp <?php echo number_format($produk['harga'],0,',','.') ?></button>
                            <?php } ?>
                                <p>
                                <?php if($produk['review']>=0 && $produk['review']<=200){ ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=200 && $produk['review']<=400) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=400 && $produk['review']<=600) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=600 && $produk['review']<=800) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=1000) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                <?php }else{ ?>

                                <?php } ?>
                                </p>
                                <p style="margin-left: 3px;"><?php echo $produk['review'] ?> reviews</p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->
