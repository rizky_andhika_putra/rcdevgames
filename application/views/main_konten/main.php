           <div class="col-md-9">

                <div class="row carousel-holder" style="border-radius:10px">

                    <div class="col-md-12">
                        <div id="RCDEV" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#RCDEV" data-slide-to="0" class="active"></li>
                                <li data-target="#RCDEV" data-slide-to="1"></li>
                                <li data-target="#RCDEV" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="assets/img/sd1.jpg" alt="" style="border-radius:7px">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="assets/img/sd2.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="assets/img/sd3.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#RCDEV" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#RCDEV" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>

                </div><hr/>
<!-- Teks Bergerak Manual -->
<h3 style="text-align:center">
<script language="JavaScript1.2">
var message="TOP 3 Game Offline - RCDev Games"
var neonbasecolor="maroon"
var neontextcolor="green"
var neontextcolor2="#DEFDD8"
var flashspeed=100      // speed of flashing in milliseconds
var flashingletters=3      // number of letters flashing in neontextcolor
var flashingletters2=1      // number of letters flashing in neontextcolor2 (0 to disable)
var flashpause=0      // the pause between flash-cycles in milliseconds
var n=0
if (document.all||document.getElementById){
document.write('<font color="'+neonbasecolor+'">')
for (m=0;m<message.length;m++)
document.write('<span id="neonlight'+m+'">'+message.charAt(m)+'</span>')
document.write('</font>')
}
else
document.write(message)
function crossref(number){
var crossobj=document.all? eval("document.all.neonlight"+number) : document.getElementById("neonlight"+number)
return crossobj
}
function neon(){
//Change all letters to base color
if (n==0){
for (m=0;m<message.length;m++)
crossref(m).style.color=neonbasecolor
}
//cycle through and change individual letters to neon color
crossref(n).style.color=neontextcolor
if (n>flashingletters-1) crossref(n-flashingletters).style.color=neontextcolor2
if (n>(flashingletters+flashingletters2)-1) crossref(n-flashingletters-flashingletters2).style.color=neonbasecolor
if (n<message.length-1)
n++
else{
n=0
clearInterval(flashing)
setTimeout("beginneon()",flashpause)
return
}
}
function beginneon(){
if (document.all||document.getElementById)
flashing=setInterval("neon()",flashspeed)
}
beginneon()
</script></h3>
<!-- end teks -->
                <hr/>

                <div class="row">

                <?php foreach ($game as $produk) { ?>
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="<?php echo base_url() .'assets/img/upload/'.$produk['gambar'] ?>" alt=""><hr>
                            <div class="ratings">
                            <h4><a href="<?php echo base_url().'product/index/'.$produk['slug'] ?>" style="padding-right: 10px;padding-left: 0px;color: #000"><?php echo $produk['nama_prod'] ?></a></h4>
                            <h5><?php echo $produk['deskripsi'] ?></h5>
                            <?php
                            if($this->session->userdata('nama_member')!=FALSE) 
                            {
                            echo form_open(base_url().'cart/add');
                            echo form_hidden('id', $produk['id_produk']);
                            echo form_hidden('name', $produk['nama_prod']);
                            echo form_hidden('price', $produk['harga']);
                            // $data['class']  = 'btn btn-success btn-sm pull-right';
                            // $data['name']   = 'action';
                            // $data['style']  = 'margin-top: 13px;';
                            // $data['value']  = 'Beli - Rp'.number_format($produk['harga'],0,',','.');
                            // echo form_submit($data);
                            echo '<button type="submit" class="btn btn-success btn-sm pull-right" style="margin-top: 13px;"><span class="fa fa-shopping-cart"></span> Beli - Rp '.number_format($produk['harga'],0,',','.') .'</button>';
                            echo form_close();
                            }else{ ?>                            
                            <button class="btn btn-success btn-sm pull-right" onclick="window.location.href='<?php echo base_url()?>member'"><span class="fa fa-shopping-cart"></span> Beli - Rp <?php echo number_format($produk['harga'],0,',','.') ?></button>
                            <?php } ?>
                                <p>
                                <?php if($produk['review']>=0 && $produk['review']<=200){ ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=200 && $produk['review']<=400) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=400 && $produk['review']<=600) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=600 && $produk['review']<=800) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                <?php }elseif ($produk['review']>=1000) { ?>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                <?php }else{ ?>

                                <?php } ?>
                                </p>
                                <p style="margin-left: 3px;"><?php echo $produk['review'] ?> reviews</p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->