<?php
class Main_model extends CI_Model {

	public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function get_kategori() {
    	$query = $this->db->get('kategori');
    	return $query->result_array();
    }

    public function get_top_game() {
        $query = $this->db->query("SELECT * FROM produk ORDER BY review DESC LIMIT 3");
        return $query->result_array();
    }

    public function list_game_home($id_kategori = FALSE) {
        if ($id_kategori === FALSE) {
            $query = $this->db->get('produk');
            return $query->result_array();
        }
            $query = $this->db->get_where('produk', array('id_kategori' => $id_kategori));
            return $query->result_array();  
    }

    public function kategori($slug) {
        $query = $this->db->get_where('kategori', array('slug' => $slug));
        $row = $query->row();
        $querys = $this->db->get_where('produk', array('id_kategori' => $row->id_kategori));
        return $querys->result_array();
    }

    public function register($data) {
    	$this->db->insert('usr_member',$data);
    }

    public function get_datagame($slug) {
        $query = $this->db->get_where('produk',array('slug' => $slug));
        return $query->row_array();
    }

    public function getpass($id, $oldpass) {
        $this->db->select('*');
        $query = $this->db->get_where("usr_member", array('id_member' => $id, 'pasword' => $oldpass));
        return $query->row_array();
    }

    public function updatepass($data) {
        $this->db->where('id_member', $data['id_member']);
        return $this->db->update('usr_member', $data);
    }

    public function update_cart($rowid, $qty, $price, $amount) {

        $data['rowid'] = $rowid;
        $data['qty'] = $qty;
        $data['price'] = $price;
        $data['amount'] = $amount;

        $this->cart->update($data);
    }

    public function getProvinsi() {
        $query = $this->db->query('SELECT * FROM lokasi WHERE lokasi_kabupatenkota = 0 and lokasi_kecamatan = 0 and lokasi_kelurahan = 0 ORDER BY lokasi_nama');
        return $query->result_array();
    }

    public function getKota($lokasi_propinsi) {
        $this->db->select('*');
        $this->db->from('lokasi');
        $this->db->where('lokasi_propinsi', $lokasi_propinsi);
        $this->db->where('lokasi_kecamatan', '0');
        $this->db->where('lokasi_kelurahan', '0');
        $this->db->where('lokasi_kabupatenkota !=', '0');
        $this->db->order_by('lokasi_nama');
        $query = $this->db->get();
        if ($query->num_rows > 0 ) {
            return $query->result_array();
        }else{
            return array();
        }
        
    }

    public function getKec($lokasi_kabupatenkota) {
        $this->db->select('*');
        $this->db->from('lokasi');
        $this->db->like('lokasi_kode', $lokasi_kabupatenkota);
        $this->db->where('lokasi_kecamatan !=', '0');
        $this->db->where('lokasi_kelurahan', '0');
        $this->db->order_by('lokasi_nama');
        $query = $this->db->get();
        if ($query->num_rows > 0 ) {
            return $query->result_array();
        }else{
            return array();
        }
        
    }

    public function get_alamat($id_alamat = FALSE) {
        if ($id_alamat === FALSE) {
            $query = $this->db->get('alamat');
            return $query->result_array();
        }
            $query = $this->db->get_where('alamat', array('id_alamat' => $id_alamat));
            return $query->row_array(); 
    }

    public function getJNE($lokasi_kode) {
        return $this->db->get_where('lokasi', array('lokasi_kode' => $lokasi_kode))->row_array();
    }
}