<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Alamats extends CI_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('nama_member')==FALSE) {
			redirect(base_url().'member');
		}
	}

	public function index() {
		show_404('page');
		// redirect()
	}

	public function detail_alamat() {
		$id_alamat = $this->input->post('alamat');
		$alamat = $this->main_model->get_alamat($id_alamat);

        echo '<div class="form-group">';
        echo '<label for="nama">Nama</label>';
        echo '<input type="text" class="form-control" name="nama" id="nama" readonly value="'.$alamat[0]['nama'].'">';
        echo '</div>';
        echo '<div class="form-group">';
        echo '<label for="email">Email</label>';
        echo '<input type="text" class="form-control" name="email" id="email" readonly value="'.$alamat[0]['email'].'">';
        echo '</div>';
        echo '<div class="form-group">';
        echo '<label for="telpon">Telpon/Handphone</label>';
        echo '<input type="text" class="form-control" name="telpon" id="telpon" readonly value="'.$alamat[0]['telpon'].'">';
        echo '</div>';
        echo '<div class="form-group">';
        echo '<label for="alamat">Alamat Lengkap</label>';
        echo '<textarea name="alamat" class="form-control" rows="7" readonly>'.$alamat[0]['alamat'].'</textarea>';
        echo '</div>';
        echo '<input type="hidden" class="form-control" name="id_alamat" id="id_alamat" readonly value="'.$alamat[0]['id_alamat'].'">';
        echo '<input type="hidden" class="form-control" name="lokasi_kode" id="lokasi_kode" readonly value="'.$alamat[0]['id_jne'].'">';
        		
	}
}
