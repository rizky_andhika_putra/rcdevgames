<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
     	parent::__construct();
     	date_default_timezone_set('Asia/Jakarta');
     }

	public function index() {
		$produk = $this->main_model->get_top_game();
		$data['title'] 	= 'RCDev Game - Play Online Game in Local Network';
		$data['side'] 	= 'main/sidebar';
		$data['game']	= $produk;
		$data['isi'] 	= 'main_konten/main';
		$this->load->view('main/wrapper',$data);
	}
}
