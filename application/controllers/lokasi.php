<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lokasi extends CI_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('nama_member')==FALSE) {
			redirect(base_url().'member');
		}
	}

	public function index() {
		show_404('page');
		// redirect()
	}

	public function getkabkot() {
		$lokasi_propinsi = $this->input->post('lokasi_propinsi');
		$kabkot = $this->main_model->getKota($lokasi_propinsi);

		echo "<option value=''>Pilih Kota/Kabupaten</option>";
		foreach ($kabkot as $kota) {
			echo "<option value='$lokasi_propinsi.$kota[lokasi_kabupatenkota].'>$kota[lokasi_nama]</option>";
		}
		
	}

	public function getkec() {
		$lokasi_kabupatenkota = $this->input->post('lokasi_kabupatenkota');
		$Kecamatan = $this->main_model->getKec($lokasi_kabupatenkota);

		echo "<option value=''>Pilih Kecamatan</option>";
		foreach ($Kecamatan as $kec) {
			echo "<option value='$kec[lokasi_kode]'>$kec[lokasi_nama]</option>";
		}
		// echo var_dump($Kecamatan);
	}

	public function reset() {
		$cart = $this->cart->contents();
		$grand_total = 0; $i = 1;
		foreach ($cart as $item) {
			$grand_total = $grand_total + $item['subtotal'];

		}
		echo "<b class='pull-right'>Rp. 0</b>";
        echo "<p>Ongkos Kirim (<b style='font-style:italic;color:blue;'>JNE</b>-<b style='color:red;' id='type_kurir'></b>)</p>";
        echo "<hr/>";
        echo "<b class='pull-right' id='total'>Rp. ".number_format($grand_total,0,',','.')."</b>";
        echo "<input type='hidden' name='grand_total' value='".$grand_total."'>";
	}

	public function getJNE() {
		$lokasi_kode = $this->input->post('lokasi_kode');
		$data = $this->main_model->getJNE($lokasi_kode);
		$cart = $this->cart->contents();
		$grand_total = 0; $i = 1;
		foreach ($cart as $item) {
			$grand_total = $grand_total + $item['subtotal'];

		}
		$total = $grand_total + $data['trf_reg'];
		echo "<b class='pull-right'>Rp. ".number_format($data['trf_reg'],0,',','.')."</b>";
        echo "<p>Ongkos Kirim (<b style='font-style:italic;color:blue;'>JNE</b>-<b style='color:red;' id='type_kurir'></b>)</p>";
        echo "<hr/>";
        echo "<b class='pull-right'>Rp. ".number_format($grand_total + $data['trf_reg'],0,',','.')."</b>";
        echo "<input type='hidden' name='ongkir' id='ongkir' value='".$data['trf_reg']."'>";
        echo "<input type='hidden' name='total' value='".$total."'>";
	}
}
