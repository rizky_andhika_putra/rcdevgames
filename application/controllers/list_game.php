<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class List_game extends CI_Controller {
		public function __construct() {
			parent::__construct();
			date_default_timezone_set('Asia/Jakarta');
		}

		public function index() {	
		$produk = $this->main_model->list_game_home();
		$data = array(	'title' => 'RCDev Games - List Game',
						'side'	=> 'main/sidebar',
						'game'	=> $produk,
						'isi'	=> 'main_konten/list' );
		$this->load->view('main/wrapper',$data);
		}

		public function kategori($slug) {	
		$produk = $this->main_model->kategori($slug);
		$data = array(	'title' => 'RCDev Games - List Game Kategori ',
						'side'	=> 'main/sidebar',
						'game'	=> $produk,
						'isi'	=> 'main_konten/list_kategori' );
		$this->load->view('main/wrapper',$data);
		}
}
