<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
	$data = array(	'title' => 'RCDev Games - Play Online Game in Local Network',
					'side'	=> 'main/sidebar',
					'isi'	=> 'main_konten/kontak' );
	$this->load->view('main/wrapper',$data);
	}
}
