<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

		public function index($slug) {
		$data_game = $this->main_model->get_datagame($slug);
		$nama_game = $data_game['nama_prod'];

		$data = array(	'title' => 'RCDev Games - '.$nama_game,
						'side'	=> 'main/sidebar',
						'data'	=> $data_game,
						'isi'	=> 'main_konten/game' );
		$this->load->view('main/wrapper',$data);
	}
}
