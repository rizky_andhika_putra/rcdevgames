<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		if($this->session->userdata('nama_member')==FALSE) {
			$this->login();
		}else{
			redirect(base_url());
		}
	}

	function register() {

		if($this->session->userdata('nama_member')==FALSE) {
			$nama_member = $this->input->post('nama_member');
			$email 		 = $this->input->post('email1');
			$pasword 	 = $this->input->post('pasword');
			$pasword1	 = $this->input->post('pasword1');
			$valid 		 = $this->form_validation;

			$valid->set_rules('nama_member','Nama Member','required|trim|xss_clean');
			$valid->set_rules('email','Email','valid_email|require|trim|xss_clean|is_unique[usr_member.email]');
			$valid->set_rules('pasword','Password', 'required|trim|xss_clean');
			$valid->set_rules('pasword1','Konfirm Password', 'required|trim|xss_clean');

			if($valid->run() === FALSE) {

				$data = array(	'title' => 'RCDev Games - Member Area',
						'side'	=> '',
						'isi'	=> 'main_konten/login' );
				$this->load->view('main/wrapper',$data);
			}else{
				if($this->input->post('pasword') !== $this->input->post('pasword1')) {
					$data = array(	'title' => 'RCDev Games - Member Area',
									'side'	=> '',
									'isi'	=> 'main_konten/login' );
					$this->load->view('main/wrapper',$data);
					$this->session->set_flashdata('failed_login', 'Opps Pasword Tidak sama!');
				}else{	
				$data = array(	'nama_member' 	=> $nama_member,
								'email'			=> $email,
								'pasword'		=> $pasword,
								'aktifasi'		=> '1' );
				$this->main_model->register($data);
				$this->session->set_flashdata('sukses', 'Selamat '.$nama_member.' Registrasi anda berhasil ! Silahkan klik untuk <a href="'.base_url().'member/">Login</a>.');
				redirect(base_url());
				}
			}

		}else{
			redirect(base_url());
		}
	}

	public function login() {
		if($this->session->userdata('nama_member')==FALSE) {
		$email = $this->input->post('email');
		$pasword = $this->input->post('pasword');
		
		$this->form_validation->set_rules('email','Email', 'required|trim|xss_clean|valid_email');
		$this->form_validation->set_rules('pasword','Password', 'required|trim|xss_clean');
		
		if($this->form_validation->run()) {
			$this->lib_login->login_member($email, $pasword);
		}
		$data = array(	'title' => 'RCDev Games - Member Area',
						'side'	=> '',
						'isi'	=> 'main_konten/login' );
		$this->load->view('main/wrapper',$data);
		}else{
			redirect(base_url());
		}
	}

	function logout() {
		$this->lib_login->logout();
	}

	public function profile() {
		if($this->session->userdata('nama_member')==FALSE) {
			$this->login();
		}else{
			// $id = $this->session->userdata('id');
			// $user = $this->main_model->getpass($id);
			$data = array(	'title' => 	'RCDev Games - Profile Member' ,
							'side'	=>	'main/sidebar' ,
							'isi'	=>	'main_konten/profil_member_editpass'
						);
			$this->load->view('main/wrapper' ,$data);
		}
	}

	function changepass() {
		if($this->session->userdata('nama_member')==FALSE) {
			$this->login();
		}else{
			$id_member = $this->input->post("id_member");
			$oldpass = $this->input->post("oldpass");
			$newpass = $this->input->post("newpass");
			$confirpass = $this->input->post("confirpass");
			$valid = $this->form_validation;

			$valid->set_rules('oldpass', 'Old Password', 'required|trim|xss_clean');
			$valid->set_rules('newpass', 'New Password', 'required|trim|xss_clean');
			$valid->set_rules('confirpass', 'Confirm New Password', 'required|trim|xss_clean');

			$id = $this->session->userdata('id');
			$user = $this->main_model->getpass($id,$oldpass);
			$pass = $user['pasword'];

			if ($valid->run() === FALSE) {
				// redirect(base_url().'member/profile');
				$data = array(	'title' => 	'RCDev Games - Profile Member' ,
								'side'	=>	'main/sidebar' ,
								'isi'	=>	'main_konten/profil_member_editpass',
								'user' 	=> 	$user
							);
				$this->load->view('main/wrapper' ,$data);
				// header("Refresh:1;url=".base_url()."member/profile/");
			}else{
				if($newpass == $confirpass && $pass == $newpass) {
					$data = array('id_member' => $id_member, 'pasword' => $newpass);
					$this->main_model->updatepass($data);
					$this->session->set_flashdata('berhasil', 'Ubah Pasword Berhasil !');
					redirect(base_url().'member/profile/');
				}else{
					$this->session->set_flashdata('gagal', 'Pasword Salah !');
					$data = array(	'title' => 	'RCDev Games - Profile Member' ,
									'side'	=>	'main/sidebar' ,
									'isi'	=>	'main_konten/profil_member_editpass'
								);
					$this->load->view('main/wrapper' ,$data);
				}
			}
		}
	}
}
