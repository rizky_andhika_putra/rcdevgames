<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('nama_member')==FALSE) {
			redirect(base_url().'member');
		}
	}

	public function index() {
		//show_404('page');
		echo $this->input->post('nama');
		echo '<br>';
		echo $this->input->post('lokasi_kode');
		echo '<br>';
		echo $this->input->post('id_alamat');
		echo '<br>';
		echo $this->input->post('kurir');
		echo '<br>';
		echo $this->input->post('emails');
		$data = array();
		$lokasi_kode = $this->input->post('lokasi_kecamatan');
		$data = $this->main_model->getJNE($lokasi_kode);
		echo json_encode($data);
		echo '<br>';
		echo var_dump($data);
		echo '<br>';
		echo var_dump($data['trf_reg']);
	}

	public function proses() {
		$po = $this->input->post('id_pesanan');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$telpon = $this->input->post('telpon');
		$alamat = $this->input->post('alamat');
		$ongkir = $this->input->post('ongkir');
		$total = $this->input->post('total');
		$grand_total = $this->input->post('grand_total');

		echo $po.'<br>';
		echo $nama.'<br>';
		echo $email.'<br>';
		echo $telpon.'<br>';
		echo '<p>'.$alamat.'</p><br>';
		echo $ongkir.'<br>';
		echo $total.'<br>';
		echo $grand_total.'<br>';
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama') == '')
		{
			$data['inputerror'][] = 'nama';
			$data['error_string'][] = 'Kolom nama kosong!';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Kolom email kosong!';
			$data['status'] = FALSE;
		}

		if($this->input->post('telpon') == '')
		{
			$data['inputerror'][] = 'telpon';
			$data['error_string'][] = 'Kolom telpon/handphone kosong!';
			$data['status'] = FALSE;
		}

		if($this->input->post('alamat') == '')
		{
			$data['inputerror'][] = 'alamat';
			$data['error_string'][] = 'Kolom alamat kosong!';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	
}
