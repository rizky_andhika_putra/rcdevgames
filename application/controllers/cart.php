<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		if($this->session->userdata('nama_member')==FALSE) {
			redirect(base_url().'member');
		}
	}

	public function index() {
		if (!$this->cart->contents()){
			$pesan = '<p>Your cart is empty!</p>';
		}else{
			$pesan = $this->session->flashdata('message');
		}

		$data['title']	= 'RCDev Games - My Cart';
		$data['side']	= 'main/sidebar';
		$data['isi']	= 'main_konten/cart';
		$data['message']= $pesan;

		$this->load->view('main/wrapper' ,$data);
	}

	public function add() {
		$data['id']		= $this->input->post('id');
		$data['name']	= $this->input->post('name');
		$data['price']	= $this->input->post('price');
		$data['qty']	= 1;

		$this->cart->insert($data);
			
		redirect(base_url().'cart');
	}

	function remove($rowid) {
		if ($rowid=="all"){
			$this->cart->destroy();
		}else{
	
			$data['rowid']		= $rowid;
			$data['qty']		= 0;

			$this->cart->update($data);
		}
		
		redirect(base_url().'cart');
	}	

	function update(){
 		foreach($this->input->post('cart') as $id => $cart)
		{			
			$price = $cart['price'];
			$amount = $price * $cart['qty'];
			
			$this->main_model->update_cart($cart['rowid'], $cart['qty'], $price, $amount);
		}
		
		redirect(base_url().'cart');
	}

	function pembayaran() {
		if($this->cart->total_items() > 0 ) {
			$provinsi = $this->main_model->getProvinsi();
			$alamat = $this->main_model->get_alamat();

			$data['title']	= 'RCDev Games - Pembayaran';
			$data['side']	= 'main/sidebar';
			$data['isi']	= 'main_konten/pembayaran';
			$data['provinsi'] = $provinsi;
			$data['alamat']	= $alamat;

			$this->load->view('main/wrapper', $data);
		}else{
			redirect(base_url().'cart');
		}
	}

	public function history() {
		$data['title']	= 'RCDev Games - My Transaction';
		$data['side']	= 'main/sidebar';
		$data['isi']	= 'main_konten/history';

		$this->load->view('main/wrapper', $data);
	}
}
