/*
SQLyog Ultimate v11.33 (32 bit)
MySQL - 10.1.8-MariaDB : Database - db_rcdev
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_rcdev` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_rcdev`;

/*Table structure for table `berita` */

DROP TABLE IF EXISTS `berita`;

CREATE TABLE `berita` (
  `id_berita` int(5) NOT NULL,
  `judul` varchar(30) NOT NULL,
  `isi` varchar(1000) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nama` varchar(30) DEFAULT NULL,
  `kategori` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `berita` */

insert  into `berita`(`id_berita`,`judul`,`isi`,`tanggal`,`nama`,`kategori`) values (42,'RF Golden Update 2232','<p>kali ini <a href=\"\">RCDev Games</a> menghadirkan game offline baru yakni RF Offline versi server 2.2.3.2 (golden update) dengan max level 66..</p>\r\n\r\n<p>banyak fitur-fitur terbaru dari versi ini seperti Gold Point lalu upgrade item seperti Tier 6 dan masih banyak lagi..</p>\r\n\r\n<p>tunggu apa lagi, pesan segera produk kami <a href=\"listgame/rf2232\">RF Golden Update Server Ver 2.2.3.2</a> selama persediaan masih ada.</p>\r\n\r\n<p>Happy Play Offline ^_^</p>\r\n','2015-06-03 00:00:00','Rizky Andhika Putra','News'),(43,'RF 2232 Custom PVP','<p>Setelah Update dari Golden Update kini <a href=\"\">RCDev Games</a> mencoba melakukan custom terhadap server rf 2232 ini, perihal banyaknya permintaan akan custom server ini, sehingga kami memutuskan untuk mencoba membuat custom pada game RF di Versi yang 1 ini.</p>\r\n\r\n<p>untuk custom kali ini kami membuat max level menjadi 70 dan banyak punya equip custom pvp dan new map custom pula..</p>\r\n\r\n<p>rasakan sensasi new item &amp; map custom di <a href=\"listgame/rf2232pvp\">RF 2232 Custom PVP</a></p>\r\n\r\n<p>Happy Playing Offline ^_^</p>\r\n','2015-06-03 00:00:00','Rizky Andhika Putra','Update'),(44,'Update Server PB PTS 8.3','<p>new update server point blank pts 8.3.</p>\r\n\r\n<ul>\r\n	<li>Fix Bugtrap Senjata.</li>\r\n	<li>Fix Map Bot yang Stuck.</li>\r\n	<li>Fix comamnd server.</li>\r\n	<li>Fix Karakter erot.</li>\r\n	<li>Fix pilih senjata dan karakter.</li>\r\n</ul>\r\n\r\n<p>Bagi yang sudah membeli tidak akan ada update gratis untuk ini, maka jika ingin update anda dapat membeli kembali produk dari&nbsp;<a href=\"http://a\">RCDev&nbsp;Games</a>&nbsp;.</p>\r\n\r\n<p>Happy Playing Offline ^_^</p>\r\n','2015-06-03 00:00:00','Rizky Andhika Putra','Update'),(47,'Buat Contoh Berita','<p>Buat Cotoh Berita Untuk test trial error wkwkwkwkw</p>\r\n','2015-06-03 09:43:44','Rizky Andhika Putra','Update');

/*Table structure for table `config` */

DROP TABLE IF EXISTS `config`;

CREATE TABLE `config` (
  `ID` int(1) NOT NULL AUTO_INCREMENT,
  `nama_web` varchar(50) NOT NULL,
  `tagline` varchar(100) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `telpon` varchar(15) DEFAULT NULL,
  `bbm` varchar(10) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `alamat` varchar(500) DEFAULT NULL,
  `iklan` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `config` */

insert  into `config`(`ID`,`nama_web`,`tagline`,`icon`,`nama`,`telpon`,`bbm`,`email`,`facebook`,`alamat`,`iklan`) values (1,'RCDev Games','Play Online Game in Local Network Now!','icon','Rizky Andhika Putra','089624234178','523D71B1','RCDev.Games@gmail.com','https://www.facebook.com/DikaMyWife','Perum Alam Pesona Wanajaya Blok P10/20 RT. 008/018 Ds. Wanajaya Kec. Cibitung Kab. Bekasi 17520','Pasang Iklan disini');

/*Table structure for table `game` */

DROP TABLE IF EXISTS `game`;

CREATE TABLE `game` (
  `id_game` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `jenis` varchar(15) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  PRIMARY KEY (`id_game`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `game` */

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` int(5) NOT NULL AUTO_INCREMENT,
  `nama_kategori` tinytext NOT NULL,
  `slug` varchar(15) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kategori`,`nama_kategori`,`slug`) values (1,'Music & Dance','music_n_dance'),(2,'First Person Shooter','first_person_sh'),(3,'Role Playing Game','role_paying_gam'),(4,'Real Time Strategy','real_time_strat');

/*Table structure for table `konfigurasi` */

DROP TABLE IF EXISTS `konfigurasi`;

CREATE TABLE `konfigurasi` (
  `name_site` text NOT NULL,
  `tagline` text NOT NULL,
  `email` text NOT NULL,
  `facebook` text NOT NULL,
  `nope1` text NOT NULL,
  `nope2` text NOT NULL,
  `bbm` text NOT NULL,
  `nama_bank1` text NOT NULL,
  `norek1` text NOT NULL,
  `nama_bank2` text NOT NULL,
  `norek2` text NOT NULL,
  `nama_bank3` text NOT NULL,
  `norek3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `konfigurasi` */

/*Table structure for table `kurir` */

DROP TABLE IF EXISTS `kurir`;

CREATE TABLE `kurir` (
  `id_kurir` int(7) NOT NULL AUTO_INCREMENT,
  `nama_kurir` tinytext,
  `gambar` longtext,
  PRIMARY KEY (`id_kurir`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kurir` */

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `nomor_po` int(8) NOT NULL AUTO_INCREMENT,
  `id_produk` int(7) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `ongkir` int(11) DEFAULT NULL,
  `nama_member` text,
  `alamat` longtext,
  `nope` int(11) DEFAULT NULL,
  `email` tinytext,
  `id_kurir` int(7) DEFAULT NULL,
  `noresi` int(15) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`nomor_po`),
  KEY `id_produk` (`id_produk`),
  KEY `id_kurir` (`id_kurir`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  CONSTRAINT `order_ibfk_2` FOREIGN KEY (`id_kurir`) REFERENCES `kurir` (`id_kurir`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `order` */

/*Table structure for table `pesan` */

DROP TABLE IF EXISTS `pesan`;

CREATE TABLE `pesan` (
  `id_pesan` int(7) NOT NULL AUTO_INCREMENT,
  `nama_guest` tinytext,
  `email` tinytext,
  `ringkas` tinytext,
  `isi` longtext,
  PRIMARY KEY (`id_pesan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pesan` */

/*Table structure for table `produk` */

DROP TABLE IF EXISTS `produk`;

CREATE TABLE `produk` (
  `id_produk` int(7) NOT NULL AUTO_INCREMENT,
  `nama_prod` longtext NOT NULL,
  `slug` tinytext NOT NULL,
  `deskripsi` longtext NOT NULL,
  `gambar` longtext NOT NULL,
  `harga` int(9) NOT NULL,
  `id_kategori` tinyint(4) NOT NULL,
  `star` int(11) DEFAULT NULL,
  `review` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_produk`),
  KEY `id_kategori` (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `produk` */

insert  into `produk`(`id_produk`,`nama_prod`,`slug`,`deskripsi`,`gambar`,`harga`,`id_kategori`,`star`,`review`) values (1,'Audition V8.3','audition_v83','ayodance offline','1.png',50000,1,3,178),(2,'RF BSB 2.2.3','rf_bsb_223','rf offline bellato strike back ver 2.2.3','2.png',50000,3,5,300),(3,'RF Golden Age 2.2.3.2','rf_ga_2232','rf offline golden age ver 2.2.3.2','3.jpg',60000,3,5,589);

/*Table structure for table `usr_admin` */

DROP TABLE IF EXISTS `usr_admin`;

CREATE TABLE `usr_admin` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `username` tinytext NOT NULL,
  `pasword` tinytext NOT NULL,
  `nama_admin` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `usr_admin` */

/*Table structure for table `usr_member` */

DROP TABLE IF EXISTS `usr_member`;

CREATE TABLE `usr_member` (
  `id_member` int(7) NOT NULL AUTO_INCREMENT,
  `nama_member` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kel` tinyint(1) DEFAULT NULL,
  `password` tinytext NOT NULL,
  `tanggal` timestamp NULL DEFAULT NULL,
  `aktifasi` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `usr_member` */

insert  into `usr_member`(`id_member`,`nama_member`,`email`,`tgl_lahir`,`jenis_kel`,`password`,`tanggal`,`aktifasi`) values (1,'Rizky Andhika Putra','rcdev.games@gmail.com','1994-11-25',1,'unityarea','2015-11-20 19:46:39',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
